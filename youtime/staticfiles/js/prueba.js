/*
    Ajax CSRF verification - Must include in all javascript file that will be used with DJANGO TEMPLATES
*/
function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
var csrftoken = getCookie("csrftoken");

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);
}

$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
  }
});



window.onload = function()
  {
       alerta = document.getElementById('alerta').value;
       if (alerta === 'True'){
        swal({
          title: "Vacaciones denegadas!",
          text: "Asegurate de no exceder los dias disponibles",
          timer: 4000,
          showConfirmButton: false
        });
      }
  }
  

//Pulsamos en entrada a trabajar y creamos una nueva entrada mediante Ajax.
$(document).ready(function() {
  $("#entrar").on("click", function() {
    swal({
      title: "Quieres entrar?",
      text: "Tu tiempo empezará a contabilizar",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willEnter => {
      if (willEnter) {
        $.ajax({
        url: "/",
        type: "POST",
        data: {
          accion: "entrar"
        },
        success: function(response) {
          var dentro = $('#alerta').val()
          if (dentro==='True'){
              
          }else{
            swal("Entrada Registrada!", "Pasa un buen dia!", "success");
          } 
          
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    }
  })
});

//Lo mismo para la salida, pulsando en el boton salir.
$("#salir").on("click", function() {
  swal({
    title: "Quieres salir?",
    text: "Tu tiempo dejará de contabilizar",
    icon: "warning",
    buttons: true,
    dangerMode: true
  }).then(willEnter => {
    if (willEnter) {
      $.ajax({
      url: "/",
      type: "POST",
      data: {
        accion: "salir"
      },
      success: function(response) {
        swal("Salida Registrada!", "Hasta pronto!", "success");
        window.location='/admin/logout/'
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(textStatus, errorThrown);
      }
    });
  }
})
});
});


//Borrar un mensaje 
$(document).on("click", "#borrar_mensaje", function() {
  var id = $(this).val();
    swal({
      title: "Estas seguro?",
      text: "Se borrará el mensaje para siempre",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        $(this).parent().parent("tr").remove();
        $.ajax({
          url: "/mensajes/",
          type: "POST",
          data: {
            accion: "borrar",
            mensaje: id
          },
          success: function(response) {
            swal("Ok", "Mensaje borrado!", "success");
          },
          error: function(xhr, ajaxOptions, thrownError) {
            swal(
              "No se ha podido borrar!",
              "Hable con un administrador",
              "error"
            );
          }
        });
      }
    });

});

//Con este boton borramos un registro de vacaciones seleccionando la linea.
$(document).on("click", "#borrar", function() {
  var aceptadas = $(this)
    .closest("td")
    .prev("td")
    .prev("td")
    .attr("value");

  var id = $(this).val();
  console.log(aceptadas);
  if (aceptadas == "False") {
    swal({
      title: "Estas seguro?",
      text: "Se actualizarán tus vacaciones!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        $(this).parent().parent("tr").remove();
        $.ajax({
          url: "/vacaciones/",
          type: "POST",
          data: {
            accion: "borrar",
            vacacion: id
          },
          success: function(response) {
            swal("Ok", "Vacaciones borradas correctamente!", "success");
          },
          error: function(xhr, ajaxOptions, thrownError) {
            swal(
              "No se ha podido borrar!",
              "Hable con un administrador",
              "error"
            );
          }
        });
      }
    });
  } else {
    swal(
      "No se pueden borrar vacaciones que ya se han confirmado",
      "póngase en contacto con el administrador"
    );
  }
});




//Modal del formulario de vacaciones
$("#boton_vacaciones").click("click", function(event) {
  $("#modal_vacaciones").modal("toggle");
});

//grafica

try {
  var ctx = document.getElementById("myChart").getContext("2d");
} catch (error) {
  console.log("No hay graficas para mostrar");
}

var puntualidad = null;

var myChart = new Chart(ctx, {
  type: "radar",
  data: {
    labels: ["puntualidad"],
    datasets: [
      {
        label: "% Puntualidad",
        data: [puntualidad],
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)",
          "rgba(255, 159, 64, 0.2)"
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)"
        ],
        borderWidth: 1
      }
    ]
  },
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  }
});
