apt-get install python3-venv
python3 -m venv env
activate () {
  env/bin/activate
}
pip3 install -r requirements.txt

mysql -u root  < youtime.sql

python3 manage.py makemigrations 
#Hacemos las migraciones para sincronizar mysql
python3 manage.py migrate

#Creamos superusuario para el uso de la aplicación
echo "from django.contrib.auth.models import User; User.objects.create_superuser('fdlr', 'fdelji3@gmail.com', 'fdlr1987')" | python3 manage.py shell
echo "from django.contrib.auth.models import User; user =  User.objects.create_user('pepito', 'pepito@pepon.com', 'pepito123'); user.is_staff=True; user.save()" | python3 manage.py shell

mysql -u root youtime -e "insert into youtime.gestor_perfil(user_id,dias_vacaciones,hora_entrada,hora_salida,dentro) values(1,22,'09:00:00','18:30:00',0);"

mysql -u root youtime -e "insert into youtime.gestor_perfil(user_id,dias_vacaciones,hora_entrada,hora_salida,dentro) values(2,22,'09:00:00','18:30:00',0);"


python3 manage.py runserver
