from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.db.models import Sum, Avg, F, ExpressionWrapper, DurationField
from .models import Perfil, Vacaciones, Entrada, Salida, Evento, Mensaje, Diario
from datetime import datetime, time, timedelta, date
from .scripts import calcularHoras
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import User
# Aqui controlamos las vistas de la app

# Vista Home de la APP (Pagina de inicio con toda la info y diario)

def home(request, *args, **kwargs):
    # primero cargamos si estamos autenticados
    if request.user.is_authenticated:
        # Obtenemos el usuario y la fecha actual.
        usuario = request.user
        hoy = date.today()
        try:
            perfil_usu = Perfil.objects.get(user=usuario.id)
            flag_dentro = perfil_usu.dentro
        except Perfil.DoesNotExist:
            perfil_usu = None
        try:
            # Comprobamos que exista un diario
            diario = Diario.objects.filter(usuario=usuario.id).latest()
        except Diario.DoesNotExist:
            diario = None
        # Contamos los mensajes nuevos Y cargamos los objetos necesarios
        mensajes = Mensaje.objects.filter(
            usuario_receptor=request.user.id).count()
        entrada = Entrada.objects.filter(user_id=usuario.id).latest
        vacas = Vacaciones.objects.filter(user=usuario.id).latest
        salida = Salida.objects.filter(user_id=usuario.id).latest
        # Post para realizar una entrada
        if request.POST.get('accion') == 'entrar':
            if flag_dentro == False:
                client_ip = request.META['REMOTE_ADDR']
                entrada = Entrada(user=request.user, ip=client_ip)
                entrada.save()
                Perfil.objects.filter(user=usuario.id).update(dentro=True)
                if diario is None:
                    diario = Diario(usuario=usuario,
                                    hora_entrada=entrada.hora, dia=date.today())
                    diario.save()
                elif diario.dia == hoy:
                    diario.horas_trabajadas = datetime.combine(
                        date.min, diario.ultima_salida) - datetime.combine(date.min,diario.hora_entrada)
                    diario.save()
                elif diario.dia < hoy:
                    diario = Diario(usuario=usuario,
                                    hora_entrada=entrada.hora, dia=date.today())
                    diario.save()
                # Si no existe un diario creamos uno nuevo con los datos
            else:
                alerta = True

        # Post para realizar una salida.
        if request.POST.get('accion') == 'salir':
            if flag_dentro is True:
                client_ip = request.META['REMOTE_ADDR']
                salida = Salida(user=request.user, ip=client_ip)
                salida.save()
                Perfil.objects.filter(user=usuario.id).update(dentro=False)
                diario = Diario.objects.filter(usuario=usuario.id).latest()
                diario.ultima_salida = salida.hora
                diario.save()
                # Si no existe un diario creamos uno nuevo con los datos
            else:
                alerta = True

        contexto = {
            'vacaciones': vacas,
            'perfil': perfil_usu,
            'usuario': usuario,
            'entrada': entrada,
            'salida': salida,
            'mensajes': mensajes,
            'diario': diario,
        }

        return render(request, "home.html", contexto)
    else:
        return HttpResponseRedirect('/admin/login/')

# vista del panel de vacaciones, desde aqui las gestionamos


def vacaciones(request, *args, **kwargs):
    if request.user.is_authenticated:
        vacaciones_usu = Vacaciones.objects.filter(
            user=request.user).order_by('-fecha_hora')
        sumDias = Vacaciones.objects.filter(
            user=request.user.id, aceptadas=True).aggregate(sumando=Sum('dias'))
        perfil = Perfil.objects.filter(user=request.user.id).update(
            dias_gastados=sumDias['sumando'])
        try:
            perfil_usu = Perfil.objects.get(user=request.user)
        except:
            perfil_usu = "Rellena tu perfil"
        contexto = {
            'usuario': request.user,
            'vacaciones': vacaciones_usu,
            'solicitudes': sumDias['sumando'],
            'perfil': perfil_usu
        }
        if request.method == 'POST':
            # Si el post es para borrar un registro de vacaciones.
            if request.POST.get('accion') == 'borrar':
                id = request.POST.get('vacacion')
                borrar = Vacaciones.objects.get(id=id)
                dias = borrar.dias
                borrar.delete()
                return render(request, "vacaciones.html", contexto)
            # Si el post es para pedir vacaciones
            fecha_desde = datetime.strptime(
                request.POST.get('fecha_desde'), "%Y-%m-%d")
            fecha_hasta = datetime.strptime(
                request.POST.get('fecha_hasta'), "%Y-%m-%d")
            days = abs((fecha_hasta - fecha_desde).days)
            # Actualizamos los dias en el perfil del Usuario
            nueva_solicitud = Vacaciones(
                fecha_desde=request.POST.get('fecha_desde'), fecha_hasta=request.POST.get('fecha_hasta'), user=request.user, dias=days)
            perfil_usu = Perfil.objects.get(user=request.user.id)
            if perfil_usu.dias_vacaciones - days <= 0:
                alerta = True
            else:
                alerta = False
                # Enviamos un email al administrador para avisarle que el usuario a solicitado vacaciones y debe aceptarlas.
                mensaje = "El usuario: {} ,ha solicitado vacaciones, puedes aceptarlas desde el gestor de vacaciones del admin".format(
                    request.user.username)
                nueva_solicitud.save()
                mensaje_admin = Mensaje(asunto="Nueva solicitud de vacaciones", usuario_receptor=User.objects.get(id=1),
                                        id_emisor=request.user.id, msg=mensaje, fecha_leido=None)
                mensaje_admin.save()

                # Enviamos el email al admin
                subject = 'Solicitud vacaciones {}'.format(
                    request.user.username)
                message = mensaje
            if nueva_solicitud.aceptadas == True:
                perfil_usu.dias_vacaciones = perfil_usu.dias_vacaciones - nueva_solicitud.dias
                perfil_usu.save()
            contexto.update({'vacaciones': vacaciones_usu, 'alerta': alerta})

        return render(request, "vacaciones.html", contexto)
    else:
        return HttpResponseRedirect('/admin/login/')

# Vista desde la que añadiremos nuestro perfil


def perfil(request, *args, **kwargs):
    if request.user.is_authenticated:
        try:
            perfil_usu = Perfil.objects.get(user=request.user)
        except:
            perfil_usu = "Rellena tu perfil"
        contexto = {
            'usuario': request.user,
            'perfil': perfil_usu,
        }
        return render(request, "perfil.html", contexto)
    else:
        return HttpResponseRedirect('/admin/login/')


# Esta es la vista de los mensajes del usuario
def mensajes(request, *args, **kwarsgs):
    if request.user.is_authenticated:
        try:
            perfil_usu = Perfil.objects.get(user=request.user)
        except:
            perfil_usu = "Rellena tu perfil"
        try:
            n_mensajes = Mensaje.objects.filter(
                usuario_receptor=request.user.id).count()
        except:
            n_mensajes = None
        try:
            t_mensajes = Mensaje.objects.filter(
                usuario_receptor=request.user.id)
        except:
            t_mensajes = None
        contexto = {
            'usuario': request.user,
            'perfil': perfil_usu,
            'mensajes': n_mensajes,
            'todos_mensajes': t_mensajes
        }
        if request.POST.get('accion') == 'borrar':
            id = request.POST.get('mensaje')
            borrar = Mensaje.objects.get(id=id).delete()
        return render(request, "mensajes.html", contexto)
    else:
        return HttpResponseRedirect('/admin/login/')
