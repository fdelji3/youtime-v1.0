from django_resized import ResizedImageField
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from PIL import Image
import sys


# Entidad Perfil (Pertenece a un usuario relacion 1-1)
class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    imagen_perfil=ResizedImageField(size=[100, 125],quality=100 ,upload_to='images/perfiles', blank=True, null=True)
    dias_vacaciones = models.PositiveIntegerField(default=22, blank=True)
    dias_gastados = models.PositiveIntegerField(default=0, blank=True,null=True)
    hora_entrada = models.TimeField(null=True, blank=True)
    hora_salida = models.TimeField(null=True, blank=True)
    dentro = models.BooleanField(default=False)
    class Meta:
        verbose_name = "Perfil"
        verbose_name_plural = "Perfiles"
    def __str__(self):
        return self.user.username


# Entidad Vacaciones
class Vacaciones(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_desde = models.DateField(null=True, blank=True)
    fecha_hasta = models.DateField(null=True, blank=True)
    aceptadas = models.BooleanField(default=False)
    fecha_hora = models.DateTimeField(auto_now_add=True, null=True)
    dias = models.IntegerField(null=True, blank=True)
    class Meta:
        verbose_name = "Vacaciones"
        verbose_name_plural = "Vacaciones"
        get_latest_by = "fecha_hora"
    def __init__(self, *args, **kwargs):
        super(Vacaciones, self).__init__(*args, **kwargs)
        self.__original_aceptadas = self.aceptadas

    def __str__(self):
        if self.aceptadas == True:
            usu = str(self.user.username) + " Ha solicitado " + str(self.dias) + \
                " de vacaciones" + " ######Estan Aceptadas"
        elif self.aceptadas is False:
            usu =str(self.user) + " Ha solicitado " + str(
                self.dias) + " de vacaciones" + " #######Estan rechazadas"
        else:
            passsrc = ""
        return usu

    def save(self, *args, **kwargs):
        perfil=Perfil.objects.get(user=self.user)
        resta=perfil.dias_vacaciones - self.dias
        if self.aceptadas != self.__original_aceptadas:
            if self.aceptadas ==True:
                if perfil.dias_gastados == None:
                    perfil.dias_gastados = 0
                perfil.dias_vacaciones -= self.dias
                perfil.dias_gastados += self.dias
                perfil.save()
            if self.aceptadas==False:
                perfil.dias_vacaciones += self.dias
                perfil.dias_gastados -= self.dias
                perfil.save()

        super(Vacaciones, self).save(*args, **kwargs)



# Entidad entrada de Usuario

class Entrada(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_hora = models.DateTimeField(auto_now_add=True, null=True)
    ip = models.GenericIPAddressField(blank=True, null=True)
    hora = models.TimeField(auto_now=True)

    class Meta:
        verbose_name = "Entrada"
        verbose_name_plural = "Entradas"
        get_latest_by = "id"

    def __str__(self):
        return self.user.username


# Entidad Salida asociada a un perfil.

class Salida(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE)
    fecha_hora = models.DateTimeField(auto_now_add=True, null=True)
    ip = models.GenericIPAddressField(blank=True, null=True)
    hora = models.TimeField(auto_now=True)

    class Meta:
        verbose_name = "Salida"
        verbose_name_plural = "Salidas"
        get_latest_by = "id"

    def __str__(self):
        return self.user.username



#Eventos para crear mensajes y alertas (Posteriormente se agregará un calendario)
class Evento(models.Model):
    PRIORIDAD = (
        ('1', 'Alta'),
        ('2', 'Media'),
        ('3', 'Baja'),
    )
    TIPO=(
        ('1', 'Vacaciones'),
        ('2', 'Tarde'),
        ('3', 'Mensaje'),
    )
    tipo_evento=models.CharField(max_length=1,choices=TIPO)
    prioridad=models.CharField(max_length=1,choices=PRIORIDAD)
    mensaje=models.CharField(max_length=200,null=True,blank=True)
    leida=models.BooleanField(default=False)

    class Meta:
        verbose_name = "Evento"
        verbose_name_plural = "Eventos"
        get_latest_by = "prioridad"


#Clase para los mensajes internos. Por ejemplo avisar al admin que tiene vacaciones pendientes de aceptar. 

class Mensaje(models.Model):
    asunto=models.CharField(max_length=50,null=True)
    usuario_receptor=models.ForeignKey(User,on_delete=models.CASCADE)
    id_emisor=models.PositiveIntegerField()
    msg=models.TextField(max_length=300)
    fecha_hora=models.DateTimeField(auto_now_add=True)
    leido=models.BooleanField(default=False)
    fecha_leido=models.DateTimeField(null=True,blank=True,default=False)

    class Meta:
        verbose_name = "Mensaje"
        verbose_name_plural = "Mensajes"
        get_latest_by = "fecha_hora"

    def __str__(self):
        return self.asunto



class Diario(models.Model):
    usuario=models.ForeignKey(User,on_delete=models.CASCADE)
    hora_entrada =models.TimeField(null=True,auto_now_add=True)
    ultima_salida =models.TimeField(default='00:00:00',null=True)
    dia=models.DateField(auto_now_add=True)
    llegada_tarde=models.BooleanField(default=False)
    horas_trabajadas=models.DurationField(null=True,default=None)
    class Meta:
        verbose_name = "Diario"
        verbose_name_plural = "Diarios"
        get_latest_by = "dia"

    def __str__(self):
        return "Dia " +str(self.dia) + " Usuario: " +  str(self.usuario)















