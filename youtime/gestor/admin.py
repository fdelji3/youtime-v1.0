from django.contrib import admin
from .models import Perfil,Vacaciones,Entrada,Salida,Evento,Mensaje,Diario
from django.http import HttpResponse
from datetime import datetime

#Administradores personalizados para los modelos de Django.
class VacacionesAdmin(admin.ModelAdmin):
    list_display = ['user','dias','fecha_hora','aceptadas']
    list_filter = ['user','aceptadas','fecha_hora']

class DiarioAdmin(admin.ModelAdmin):
    list_display = ['usuario','dia','hora_entrada','ultima_salida']
    list_filter = ['usuario','dia','llegada_tarde']
    actions = ['export_excel']

    def export_excel(self, request, queryset):
        import xlwt
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=exportacion_diarios.xls'
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet("MyModel")
    
        row_num = 0
    
        columns = [
            (u"ID", 2000),
            (u"Usuario", 6000),
            (u"Dia", 8000),
            (u"Hora de entrada", 8000),
            (u"Ultima salida", 8000),
            (u"LLegada tarde", 8000),
            (u"Horas trabajadas", 8000),
        ]

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num][0], font_style)
        # set column width
            ws.col(col_num).width = columns[col_num][1]

        font_style = xlwt.XFStyle()
        font_style.alignment.wrap = 1
    
        for obj in queryset:
            #Formateamos las fechas 
            if obj.llegada_tarde == True:
                llegada_tarde ='Si'
            else:
                llegada_tarde = 'No'
            row_num += 1
            row = [
                obj.pk,
                obj.usuario.username,
                str(obj.dia),
                str(obj.hora_entrada),
                str(obj.ultima_salida),
                str(llegada_tarde),
                str(obj.horas_trabajadas)
            ]
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)
            
        wb.save(response)
        return response
    
    export_excel.short_description = u"Exportar a EXCEL"

#Registramos los modelos en el admin
admin.site.register(Perfil)
admin.site.register(Vacaciones,VacacionesAdmin)
admin.site.register(Entrada)
admin.site.register(Salida)
admin.site.register(Evento)
admin.site.register(Mensaje)
admin.site.register(Diario,DiarioAdmin)

